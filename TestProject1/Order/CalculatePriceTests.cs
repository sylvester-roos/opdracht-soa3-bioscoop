﻿using SOA3Cinema.Domain;
using SOA3Cinema.Tests.support.fixtures;

namespace SOA3Cinema.Tests;

public class CalculatePriceTests
{
    [Theory]
    [InlineData(true, 10)]
    [InlineData(false, 20)]
    public void CalculatePriceGivesSecondTicketFreeForStudentOnTheWeekends(bool isStudentOrder, double expectedPrice)
    {
        // Arrange

        // Create Movie
        //Movie movie = new Movie("james bond");
        var movie = MovieFixture.BuildMovie();

        // Create MovieScreening
        var dateOfARandomSaturyday = new DateTime(2024, 1, 27);
        var pricePerSeat = 10.00;
        var movieScreening = new MovieScreening(movie, dateOfARandomSaturyday, pricePerSeat);

        // Create MovieTicket
        var isPremium = false;
        var ticket1 = new MovieTicket(movieScreening, isPremium, 1, 1);
        var ticket2 = new MovieTicket(movieScreening, isPremium, 1, 2);

        // Create Order
        var order = new Order(123, isStudentOrder);
        order.AddSeatReservation(ticket1);
        order.AddSeatReservation(ticket2);

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(expectedPrice, result);
    }

    [Theory]
    [InlineData(24, 10)] // 2024-01-27 is on wednesday
    [InlineData(25, 10)] // 2024-01-27 is on thursday
    [InlineData(26, 20)] // 2024-01-27 is on friday
    [InlineData(27, 20)] // 2024-01-27 is on saturday
    [InlineData(28, 20)] // 2024-01-27 is on sunday
    [InlineData(29, 10)] // 2024-01-27 is on monday
    [InlineData(30, 10)] // 2024-01-30 is on tuesday
    public void CalculatePriceGivesSecondTicketFreeForNonStudentOnMondayTillThursday(int day, double expectedPrice)
    {
        // Arrange

        // Create Movie
        var movie = new Movie("james bond");

        // Create MovieScreening
        var dateOfARandomSaturyday = new DateTime(2024, 1, day);
        var pricePerSeat = 10.00;
        var movieScreening = new MovieScreening(movie, dateOfARandomSaturyday, pricePerSeat);

        // Create MovieTicket
        var isPremium = false;
        var ticket1 = new MovieTicket(movieScreening, isPremium, 1, 1);
        var ticket2 = new MovieTicket(movieScreening, isPremium, 1, 2);

        // Create Order
        var isStudentOrder = false;
        var order = new Order(123, isStudentOrder);
        order.AddSeatReservation(ticket1);
        order.AddSeatReservation(ticket2);

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(expectedPrice, result);
    }

    [Theory]
    [InlineData(5, 50)] // 5 non-students & weekend -> no groupdiscount -> 5 * 10 = 50
    [InlineData(6, 54)] // 6 non-student & weekend -> groupdiscount -> 6 * 10 * 0.9 = 54
    [InlineData(7, 63)] // 7 non-student & weekend -> groupdiscount -> 7 * 10 * 0.9 = 63
    public void CalculatePriceGivesGroupDiscountIfSixPeopleOrMore(int amountOfPeople, double expectedPrice)
    {
        // Arrange

        // Create Movie
        var movie = new Movie("james bond");

        // Create MovieScreening
        var dateOfARandomSaturyday = new DateTime(2024, 1, 27); // a random saturday / day in the weekend
        var pricePerSeat = 10.00;
        var movieScreening = new MovieScreening(movie, dateOfARandomSaturyday, pricePerSeat);

        // Create MovieTicket
        var isPremium = false;
        var ticket1 = new MovieTicket(movieScreening, isPremium, 1, 1);
        var ticket2 = new MovieTicket(movieScreening, isPremium, 1, 2);
        var ticket3 = new MovieTicket(movieScreening, isPremium, 1, 3);
        var ticket4 = new MovieTicket(movieScreening, isPremium, 1, 4);
        var ticket5 = new MovieTicket(movieScreening, isPremium, 1, 5);
        var ticket6 = new MovieTicket(movieScreening, isPremium, 1, 6);
        var ticket7 = new MovieTicket(movieScreening, isPremium, 1, 7);

        // Create Order
        var isStudentOrder = false;
        var order = new Order(123, isStudentOrder);
        order.AddSeatReservation(ticket1);
        order.AddSeatReservation(ticket2);
        order.AddSeatReservation(ticket3);
        order.AddSeatReservation(ticket4);
        order.AddSeatReservation(ticket5);
        if ( amountOfPeople >= 6 )
        {
            order.AddSeatReservation(ticket6);
            if ( amountOfPeople == 7 )
            {
                order.AddSeatReservation(ticket7);
            }
        }


        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(expectedPrice, result);
    }

    [Theory]
    [InlineData(true, 12)]
    [InlineData(false, 13)]
    public void CalculatePriceOfPremiumTicket(bool isStudentOrder, double expectedPrice)
    {
        // Arrange

        // Create Movie
        var movie = new Movie("james bond");

        // Create MovieScreening
        var dateOfARandomSaturyday = new DateTime(2024, 1, 27);
        var pricePerSeat = 10.00;
        var movieScreening = new MovieScreening(movie, dateOfARandomSaturyday, pricePerSeat);

        // Create MovieTicket
        var isPremium = true;
        var ticket1 = new MovieTicket(movieScreening, isPremium, 1, 1);

        // Create Order
        var order = new Order(123, isStudentOrder);
        order.AddSeatReservation(ticket1);

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(expectedPrice, result);
    }

    [Theory]
    [InlineData(true, 12)]
    [InlineData(false, 26)]
    public void CalculatePriceOfPremiumTicketInCombinationWithTwoTickets(bool isStudentOrder, double expectedPrice)
    {
        // Arrange

        // Create Movie
        var movie = new Movie("james bond");

        // Create MovieScreening
        var dateOfARandomSaturyday = new DateTime(2024, 1, 27);
        var pricePerSeat = 10.00;
        var movieScreening = new MovieScreening(movie, dateOfARandomSaturyday, pricePerSeat);

        // Create MovieTicket
        var isPremium = true;
        var ticket1 = new MovieTicket(movieScreening, isPremium, 1, 1);
        var ticket2 = new MovieTicket(movieScreening, isPremium, 1, 2);

        // Create Order
        var order = new Order(123, isStudentOrder);
        order.AddSeatReservation(ticket1);
        order.AddSeatReservation(ticket2);

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(expectedPrice, result);
    }

    [Theory]
    [InlineData(5, 65)] // 5 non-students & weekend & premium -> no groupdiscount -> 5 * (10+3) = 50
    [InlineData(6, 70.2)] // 6 non-student & weekend & premium -> groupdiscount -> 6 * (10+3) * 0.9 = 54
    [InlineData(7, 81.9)] // 7 non-student & weekend & premium -> groupdiscount -> 7 * (10+3) * 0.9 = 63
    public void CalculatePriceOfPremiumTicketInCombinationWithGroupDiscount(int amountOfPeople, double expectedPrice)
    {
        // Arrange

        // Create Movie
        var movie = new Movie("james bond");

        // Create MovieScreening
        var dateOfARandomSaturyday = new DateTime(2024, 1, 27); // a random saturday / day in the weekend
        var pricePerSeat = 10.00;
        var movieScreening = new MovieScreening(movie, dateOfARandomSaturyday, pricePerSeat);

        // Create MovieTicket
        var isPremium = true;
        var ticket1 = new MovieTicket(movieScreening, isPremium, 1, 1);
        var ticket2 = new MovieTicket(movieScreening, isPremium, 1, 2);
        var ticket3 = new MovieTicket(movieScreening, isPremium, 1, 3);
        var ticket4 = new MovieTicket(movieScreening, isPremium, 1, 4);
        var ticket5 = new MovieTicket(movieScreening, isPremium, 1, 5);
        var ticket6 = new MovieTicket(movieScreening, isPremium, 1, 6);
        var ticket7 = new MovieTicket(movieScreening, isPremium, 1, 7);

        // Create Order
        var isStudentOrder = false;
        var order = new Order(123, isStudentOrder);
        order.AddSeatReservation(ticket1);
        order.AddSeatReservation(ticket2);
        order.AddSeatReservation(ticket3);
        order.AddSeatReservation(ticket4);
        order.AddSeatReservation(ticket5);
        if ( amountOfPeople >= 6 )
        {
            order.AddSeatReservation(ticket6);
            if ( amountOfPeople == 7 )
            {
                order.AddSeatReservation(ticket7);
            }
        }

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(expectedPrice, result);
    }

    [Fact]
    public void CalculateTicketPriceWhenThereAreNoTickets()
    {
        // Arrange
        var order = OrderFixture.BuildOrder();
        order.movieTickets.Clear();

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(0, result);
    }

    [Fact]
    public void CalculateTicketPriceWithSixTicketsInTheWeekendAsNonStudents()
    {
        // Arrange
        var movieScreening = MovieScreeningFixture.BuildMovieScreening(new DateTime(2024, 01, 06));
        var movieTicket = MovieTicketFixture.BuildMovieTicket(movieScreening);
        var order = OrderFixture.BuildOrder();
        order.movieTickets.Clear();
        for ( var i = 1; i <= 6; i++ )
        {
            order.AddSeatReservation(movieTicket);
        }

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(54, result);
    }

    [Fact]
    public void CalculateTicketPriceWithSixTicketsNotInTheWeekendAsNonStudents()
    {
        // Arrange
        var movieScreening = MovieScreeningFixture.BuildMovieScreening(new DateTime(2024, 01, 05));
        var movieTicket = MovieTicketFixture.BuildMovieTicket(movieScreening);
        var order = OrderFixture.BuildOrder();
        order.movieTickets.Clear();
        for ( var i = 1; i <= 6; i++ )
        {
            order.AddSeatReservation(movieTicket);
        }

        // Act
        var result = order.CalculatePrice();

        // Assert
        Assert.Equal(60, result);
    }
}
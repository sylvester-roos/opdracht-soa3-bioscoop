using SOA3Cinema.Domain;
using SOA3Cinema.Tests.support.fixtures;

namespace SOA3Cinema.Tests;

public class ExportTests
{
    [Fact]
    public void OrderExportTestWithExportFormatPLAINTEXT()
    {
        // Arrange
        var order1 = OrderFixture.BuildOrder();

        // Act
        order1.Export();

        // Assert
        var tempPath = Path.GetTempPath();
        var filePath = Path.Combine(
            tempPath,
            "order.txt"
        );
        var result = File.Exists(filePath);

        Assert.True(result);
    }

    [Theory]
    [InlineData(TicketExportFormat.PLAINTEXT)]
    [InlineData(TicketExportFormat.JSON)]
    public void OrderExportTestWithExportFormatJSON(TicketExportFormat exportFormat)
    {
        // Arrange
        var order1 = OrderFixture.BuildOrder();
        order1.SwitchExportStrategy(TicketExportFormat.JSON);
        // Act
        order1.Export();

        // Assert
        var tempPath = Path.GetTempPath();
        var filePath = Path.Combine(
            tempPath,
            "order.json"
        );
        var result = File.Exists(filePath);

        Assert.True(result);
    }
}
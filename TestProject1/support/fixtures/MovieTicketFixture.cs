using SOA3Cinema.Domain;

namespace SOA3Cinema.Tests.support.fixtures;

internal static class MovieTicketFixture
{
    public static MovieTicket BuildMovieTicket(MovieScreening? movieScreening = null)
    {
        // var screeningDate = movieScreeningDate ?? DateTime.Now;
        var screening = movieScreening ?? MovieScreeningFixture.BuildMovieScreening();

        return new MovieTicket(
            screening,
            false,
            1,
            1
        );
    }
}
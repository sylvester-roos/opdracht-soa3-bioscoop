using SOA3Cinema.Domain;

namespace SOA3Cinema.Tests.support.fixtures;

internal static class MovieScreeningFixture
{
    public static MovieScreening BuildMovieScreening(DateTime? movieScreeningDate = null)
    {
        var screeningDate = movieScreeningDate ?? DateTime.Now;

        return new MovieScreening(
            MovieFixture.BuildMovie(),
            screeningDate,
            10.0
        );
    }
}
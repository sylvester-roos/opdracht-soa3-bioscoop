using SOA3Cinema.Domain;

namespace SOA3Cinema.Tests.support.fixtures;

internal static class OrderFixture
{
    public static Domain.Order BuildOrder()
    {
        var order = new Domain.Order(
            1,
            false
        );
        order.AddSeatReservation(MovieTicketFixture.BuildMovieTicket());

        return order;
    }
}
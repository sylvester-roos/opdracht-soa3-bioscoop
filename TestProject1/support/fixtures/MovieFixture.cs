using SOA3Cinema.Domain;

namespace SOA3Cinema.Tests.support.fixtures;

internal static class MovieFixture
{
    public static Movie BuildMovie()
    {
        return new Movie("Pulp Fiction");
    }
}
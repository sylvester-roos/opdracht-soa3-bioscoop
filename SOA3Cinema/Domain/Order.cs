using SOA3Cinema.Domain.State;
using SOA3Cinema.Domain.Strategies;
using SOA3Cinema.Domain.Strategies.Abstracts;
using SOA3Cinema.Export;
using SOA3Cinema.Export.Abstracts;

namespace SOA3Cinema.Domain;

internal class Order
{

    private IState orderState { get; set; }
    internal bool isStudentOrder { get; set; }
    internal LinkedList<MovieTicket> movieTickets { get; } = new();
    internal int orderNr { get; set; }
    private ICalculatePriceStrategy calculatePriceStrategy { get; }
    private IOrderExportStrategy orderExportStrategy { get; set; }

    public Order(int orderNr, bool isStudentOrder)
    {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;

        // logica voor bepalen orderstrategy op basis van parameters (in private method)
        calculatePriceStrategy = DeterminePriceStrategy(isStudentOrder);

        // plaintext is default export strategy
        orderExportStrategy = new PlainTextExportStrategy();

        this.orderState = new OrderCreatedState(this);
    }

    private ICalculatePriceStrategy DeterminePriceStrategy(bool isStudentOrder)
    {
        if ( isStudentOrder )
        {
            return new StudentPriceStrategy(movieTickets);
        }

        return new DefaultPriceStrategy(movieTickets);
    }


    public void AddSeatReservation(MovieTicket ticket)
    {
        if ( ticket != null ) movieTickets.AddFirst(ticket);
    }

    public double CalculatePrice()
    {
        if ( movieTickets.First == null )
        {
            return 0;
        }

        // voer calculateprice() van strategy attribuut uit
        return calculatePriceStrategy.CalculatePrice();
    }

    public void SwitchExportStrategy(TicketExportFormat exportFormat)
    {
        switch ( exportFormat )
        {
            case TicketExportFormat.PLAINTEXT:
                orderExportStrategy = new PlainTextExportStrategy();
                break;
            case TicketExportFormat.JSON:
                orderExportStrategy = new JsonExportStrategy();
                break;
            default:
                throw new ArgumentException("Unsupported export format");
        }
    }


    public void SubmitOrder()
    {
        this.orderState.SubmitOrder();
    }

    public void PayOrder()
    {
        this.orderState.PayOrder();
    }

    public void SendReminder()
    {
        this.orderState.SendReminder();
    }

    public void CancelOrder()
    {
        this.orderState.CancelOrder();
    }

    internal void SetState(IState state)
    {
        this.orderState = state;
    }










    public void Export()
    {
        orderExportStrategy.Export(this);
    }
}
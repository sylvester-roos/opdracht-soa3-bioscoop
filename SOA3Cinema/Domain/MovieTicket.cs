﻿namespace SOA3Cinema.Domain;

internal class MovieTicket
{
    private readonly bool _isPremium;
    private readonly MovieScreening _movieScreening;
    private int _rowNr;
    private int _seatNr;

    public MovieTicket(MovieScreening movieScreening, bool isPremium, int seatRow, int seatNr)
    {
        _movieScreening = movieScreening;
        _rowNr = seatRow;
        _seatNr = seatNr;
        _isPremium = isPremium;
    }

    public void SelectSeat(int rowNr, int seatNr)
    {
        _rowNr = rowNr;
        _seatNr = seatNr;
    }

    public DateTime GetDate()
    {
        return _movieScreening.GetDate();
    }

    public bool IsPremiumTicket()
    {
        return _isPremium;
    }

    public double GetPrice()
    {
        return _movieScreening.GetPricePerSeat();
    }
}
namespace SOA3Cinema.Domain.State
{
    internal class PaidOrderState : IState
    {
        private Order order;

        public PaidOrderState(Order order)
        {
            this.order = order;
        }

        void IState.CancelOrder()
        {
            Console.WriteLine("Order already paid");
        }

        void IState.PayOrder()
        {
            Console.WriteLine("Order already paid");
        }

        void IState.SendReminder()
        {
            Console.WriteLine("Order already paid");
        }

        void IState.SubmitOrder()
        {
            Console.WriteLine("Order already paid");
        }
    }
}
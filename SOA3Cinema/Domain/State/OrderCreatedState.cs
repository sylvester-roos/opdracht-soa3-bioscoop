namespace SOA3Cinema.Domain.State;

internal class OrderCreatedState : IState
{
    private Order order;

    public OrderCreatedState(Order order)
    {
        this.order = order;
    }

    void IState.CancelOrder()
    {
        Console.WriteLine("Dit order bestaat nog niet, dus kan ook nog niet gecanceld worden");
    }

    void IState.PayOrder()
    {
        Console.WriteLine("Dit order is nog niet gesubmit, daarna kan er pas betaald worden");
    }

    void IState.SendReminder()
    {
        Console.WriteLine("Dit order is nog niet gesubmit");
    }

    void IState.SubmitOrder()
    {
        Console.WriteLine("Order is nu gesubmit");
        this.order.SetState(new SubmittedOrderState(this.order));
    }
}
namespace SOA3Cinema.Domain.State;

internal class SubmittedOrderState : IState
{
    private Order order;

    public SubmittedOrderState(Order order)
    {
        this.order = order;
    }

    void IState.CancelOrder()
    {
        Console.WriteLine("Order canceled");
        this.order.SetState(new CanceledOrderState(this.order));
    }

    void IState.PayOrder()
    {
        Console.WriteLine("Order paid");
        this.order.SetState(new PaidOrderState(this.order));
    }

    void IState.SendReminder()
    {
        Console.WriteLine("Reminder sent");
    }

    void IState.SubmitOrder()
    {
        Console.WriteLine("Order already submitted");
    }
}

namespace SOA3Cinema.Domain.State
{
    internal class CanceledOrderState : IState
    {
        private Order order;

        public CanceledOrderState(Order order)
        {
            this.order = order;
        }

        void IState.CancelOrder()
        {
            Console.WriteLine("Order already canceled");
        }

        void IState.PayOrder()
        {
            Console.WriteLine("Order already canceled");
        }

        void IState.SendReminder()
        {
            Console.WriteLine("Order already canceled");
        }

        void IState.SubmitOrder()
        {
            Console.WriteLine("Order already canceled");
        }
    }
}
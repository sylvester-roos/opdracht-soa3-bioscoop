namespace SOA3Cinema.Domain.State;

internal interface IState
{
    void SubmitOrder();
    void PayOrder();
    void SendReminder();
    void CancelOrder();
}
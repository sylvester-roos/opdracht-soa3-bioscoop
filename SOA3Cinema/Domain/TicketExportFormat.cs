﻿namespace SOA3Cinema.Domain
{
    public enum TicketExportFormat
    {
        PLAINTEXT,
        JSON
    }

}

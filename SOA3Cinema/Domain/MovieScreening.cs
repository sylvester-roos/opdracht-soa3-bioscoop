﻿namespace SOA3Cinema.Domain;

internal class MovieScreening
{
    private readonly DateTime _date;
    private Movie _movie;
    private readonly double _pricePerSeat;

    public MovieScreening(Movie movie, DateTime date, double pricePerSeat)
    {
        _movie = movie;
        _date = date;
        _pricePerSeat = pricePerSeat;
    }

    public DateTime GetDate()
    {
        return _date;
    }

    public double GetPricePerSeat()
    {
        return _pricePerSeat;
    }

    public override string ToString()
    {
        return "";
    }
}
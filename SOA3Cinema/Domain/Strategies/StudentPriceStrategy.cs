using SOA3Cinema.Domain.Strategies.Abstracts;

namespace SOA3Cinema.Domain.Strategies
{
    internal class StudentPriceStrategy : ICalculatePriceStrategy
    {
        private readonly LinkedList<MovieTicket> movieTickets;
        private readonly double premiumAddition = 2;
        public StudentPriceStrategy(LinkedList<MovieTicket> movieTickets)
        {
            this.movieTickets = movieTickets;
        }
        double ICalculatePriceStrategy.CalculatePrice()
        {
            // 2de ticket gratis
            int amountOfPaidTickets = this.movieTickets.Count / 2 + this.movieTickets.Count % 2;

            var pricePerSeat = movieTickets.First!.Value.GetPrice();

            // als het premium is -> ticketprice + ticketamount * toeslag
            if ( movieTickets.First!.Value.IsPremiumTicket() )
            {
                return amountOfPaidTickets * pricePerSeat + amountOfPaidTickets * premiumAddition;
            }
            return amountOfPaidTickets * pricePerSeat;
        }

    }
}

namespace SOA3Cinema.Domain.Strategies.Abstracts
{
    internal interface ICalculatePriceStrategy
    {
        double CalculatePrice();
    }
}
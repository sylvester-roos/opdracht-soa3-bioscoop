using SOA3Cinema.Domain.Strategies.Abstracts;

namespace SOA3Cinema.Domain.Strategies
{
    internal class DefaultPriceStrategy : ICalculatePriceStrategy
    {
        private readonly LinkedList<MovieTicket> movieTickets;
        private bool GroupDiscountActive = false;
        private readonly double premiumAddition = 3;
        private double totalAdditionForPremium = 0;
        public DefaultPriceStrategy(LinkedList<MovieTicket> movieTickets)
        {
            this.movieTickets = movieTickets;
        }

        double ICalculatePriceStrategy.CalculatePrice()
        {
            // if ma/di/wo/do -> 2de ticket gratis
            int amountOfPaidTickets = this.movieTickets.Count;
            if ( IsMondayToThursday() )
            {
                amountOfPaidTickets = this.movieTickets.Count / 2 + this.movieTickets.Count % 2;
            }

            if ( IsInWeekend() )
            {
                this.GroupDiscountActive = movieTickets.Count >= 6;
            }

            // als het premium is -> ticketprice + ticketamount * toeslag
            if ( movieTickets.First!.Value.IsPremiumTicket() )
            {
                totalAdditionForPremium = amountOfPaidTickets * premiumAddition;
            }

            var pricePerSeat = movieTickets.First.Value.GetPrice();

            var totalPrice = amountOfPaidTickets * pricePerSeat + totalAdditionForPremium;
            if ( GroupDiscountActive )
            {
                return totalPrice * 0.9;
            }
            return totalPrice;
        }

        private bool IsMondayToThursday()
        {
            var day = movieTickets.First!.Value.GetDate().DayOfWeek;
            return day >= DayOfWeek.Monday && day <= DayOfWeek.Thursday;
        }

        private bool IsInWeekend()
        {
            var day = movieTickets.First!.Value.GetDate().DayOfWeek;
            return day == DayOfWeek.Saturday || day == DayOfWeek.Sunday;
        }
    }
}

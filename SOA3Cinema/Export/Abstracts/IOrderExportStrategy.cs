using SOA3Cinema.Domain;

namespace SOA3Cinema.Export.Abstracts
{
    internal interface IOrderExportStrategy
    {
        void Export(Order order);
    }
}

using SOA3Cinema.Domain;
using SOA3Cinema.Export.Abstracts;
using System.Text;

namespace SOA3Cinema.Export
{
    internal class PlainTextExportStrategy : IOrderExportStrategy
    {
        public void Export(Order order)
        {
            var tempPath = Path.GetTempPath();
            var filePath = Path.Combine(tempPath, "order.txt");

            if ( File.Exists(filePath) ) File.Delete(filePath);

            var sb = new StringBuilder();
            sb.AppendLine("OrderNr: " + order.orderNr);
            sb.AppendLine("IsStudentOrder: " + order.isStudentOrder);
            sb.AppendLine("Tickets: ");
            foreach ( var ticket in order.movieTickets ) sb.AppendLine(ticket.ToString());
            sb.AppendLine("Total price: " + order.CalculatePrice());

            File.WriteAllText(filePath, sb.ToString());
        }
    }
}

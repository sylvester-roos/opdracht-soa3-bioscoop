using SOA3Cinema.Domain;
using SOA3Cinema.Export.Abstracts;
using System.Text.Json;

namespace SOA3Cinema.Export
{
    internal class JsonExportStrategy : IOrderExportStrategy
    {
        public void Export(Order order)
        {
            var tempPath = Path.GetTempPath();
            var filePath = Path.Combine(tempPath, "order.json");

            if ( File.Exists(filePath) ) File.Delete(filePath);

            var orderData = new
            {
                order.orderNr,
                order.isStudentOrder,
                tickets = order.movieTickets.Select(ticket => new
                {
                    isPremiumTicket = ticket.IsPremiumTicket(),
                    price = ticket.GetPrice(),
                    date = ticket.GetDate()
                }),
                totalPrice = order.CalculatePrice()
            };

            var json = JsonSerializer.Serialize(orderData);

            File.WriteAllText(filePath, json);
        }
    }
}
